<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function() {

    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function() {
        Route::get('user-points',['as' => 'user.points' ,'uses' => 'PublicController@UserPoints']);
    });

    Route::post('admin/login',['as' => 'admin.login', 'uses' => 'AdminController@login']);
    Route::post('branch/login',['as' => 'branch.login', 'uses' => 'BranchController@login']);

    Route::any('admin/activate/{email_token}', ['as' => 'admin.activate', 'uses' => 'AdminController@activate']);
    Route::any('admin/forget-password', ['as' => 'admin.forget.password', 'uses' => 'AdminController@forgetPassword']);
    Route::any('admin/reset-password/{email_token}', ['as' => 'admin.reset.password', 'uses' => 'AdminController@resetPassword']);

    Route::get('data-search', ['as' => 'data.search', 'uses' => 'PublicController@dataSearch']);
    Route::get('sponsors', ['as' => 'sponsors', 'uses' => 'PublicController@sponsors']);
    Route::get('merchants', ['as' => 'merchants', 'uses' => 'PublicController@merchants']);
    Route::get('branches/{id}', ['as' => 'branches', 'uses' => 'PublicController@branches'])->where('id','[0-9]+');
    Route::get('merchant/{id}', ['as' => 'merchant', 'uses' => 'PublicController@merchant'])->where('id','[0-9]+');
    Route::get('branch/{id}', ['as' => 'branch', 'uses' => 'PublicController@branch'])->where('id','[0-9]+');
    Route::get('merchant/types', ['as' => 'merchant.types', 'uses' => 'PublicController@merchantTypes']);
    Route::get('categories', ['as' => 'categories', 'uses' => 'PublicController@categories']);
    Route::get('cities', ['as' => 'cities', 'uses' => 'PublicController@cities']);
    Route::get('areas/{id}', ['as' => 'areas', 'uses' => 'PublicController@areas'])->where('id','[0-9]+');
    Route::get('voucher/list', ['as' => 'vouchers', 'uses' => 'PublicController@vouchers']);

    Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
        Route::post('store', ['as' => 'store', 'uses' => 'UserController@storeUser']);
        Route::put('{id}', ['as' => 'update', 'uses' => 'UserController@updateUser'])->where('id','[0-9]+');
    });

    # Admin DashBoard ============
    Route::group(['middleware' => 'auth:admin_api'], function() {

        Route::group(['prefix' => 'admin/reports', 'as' => 'admin.reports.'], function() {
            Route::get('user-points',['as' => 'user.points' ,'uses' => 'ReportsController@UserPoints']);
        });

        Route::group(['prefix' => 'admin/user', 'as' => 'admin.user.'], function() {
            Route::post('points', ['as' => 'store', 'uses' => 'AdminController@addPoints']);
            Route::post('assign-voucher', ['as' => 'assign.voucher', 'uses' => 'AdminController@assignVoucher']);
            Route::post('assign-event', ['as' => 'assign.event', 'uses' => 'AdminController@assignEvent']);
            Route::get('list', ['as' => 'list', 'uses' => 'AdminController@users']);
            Route::get('{id}', ['as' => 'info', 'uses' => 'AdminController@userInfo'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
            Route::get('list', ['as' => 'merchant.types', 'uses' => 'AdminController@admins']);
            Route::get('types', ['as' => 'merchant.types', 'uses' => 'AdminController@adminTypes']);
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeAdmin']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateAdmin'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'merchant', 'as' => 'merchant.'], function() {
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeMerchant']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateMerchant'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'branch', 'as' => 'branch.'], function() {
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeBranch']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateBranch'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'category', 'as' => 'category.'], function() {
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeCategory']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateCategory'])->where('id','[0-9]+');
            Route::delete('{id}', ['as' => 'delete', 'uses' => 'AdminController@deleteCategory'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'city', 'as' => 'city.'], function() {
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeCity']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateCity'])->where('id','[0-9]+');
            Route::delete('{id}', ['as' => 'delete', 'uses' => 'AdminController@deleteCity'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'area', 'as' => 'area.'], function() {
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeArea']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateArea'])->where('id','[0-9]+');
            Route::delete('{id}', ['as' => 'delete', 'uses' => 'AdminController@deleteArea'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'voucher', 'as' => 'voucher.'], function() {
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeVoucher']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateVoucher'])->where('id','[0-9]+');
            Route::delete('{id}', ['as' => 'delete', 'uses' => 'AdminController@deleteVoucher'])->where('id','[0-9]+');
        });

        Route::group(['prefix' => 'event', 'as' => 'event.'], function() {
            Route::get('list', ['as' => 'list', 'uses' => 'AdminController@events']);
            Route::post('store', ['as' => 'store', 'uses' => 'AdminController@storeEvent']);
            Route::put('{id}', ['as' => 'update', 'uses' => 'AdminController@updateEvent'])->where('id','[0-9]+');
            Route::delete('{id}', ['as' => 'delete', 'uses' => 'AdminController@deleteEvent'])->where('id','[0-9]+');
        });

    });


    # Branch DashBoard ============
    Route::group(['middleware' => 'auth:branch_api', 'prefix' => 'branch'], function() {

        Route::group(['prefix' => '', 'as' => 'branch.'], function() {
            Route::get('vouchers', ['as' => 'vouchers', 'uses' => 'BranchController@vouchers']);
            Route::get('me', ['as' => 'me', 'uses' => 'BranchController@me']);
            Route::post('redeem-voucher', ['as' => 'redeem.voucher', 'uses' => 'BranchController@redeemVoucher']);
        });

    });
});

