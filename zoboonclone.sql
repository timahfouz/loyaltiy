-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 25, 2019 at 06:10 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoboonclone`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_phone_unique` (`phone`),
  UNIQUE KEY `email_token` (`email_token`),
  KEY `admins_type_id_foreign` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `type_id`, `name`, `email`, `phone`, `image`, `password`, `firebase`, `status`, `deleted_at`, `email_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 5, 'Nabda', 'super-admin@nabda.com', '01114254513', '1576764032ZU9jJ.jpg', '$2y$10$yi/5pIow4B4xd4lNxQkbnOgmRDNRGNhnzY6K1rx9wnz.oy4FnXMZO', NULL, 1, NULL, NULL, NULL, '2019-12-19 12:45:52', '2019-12-19 12:00:33'),
(3, 5, 'Tarek Mahfouz', 'timahfouz262@gmail.com', '01273280587', NULL, '$2y$10$jL9SloUj5JSkWyTdIt4Jcu5FYWrpAi1Y7xEFlREYt15MxHe3opS9O', NULL, 1, NULL, NULL, '', '2019-12-19 13:52:53', '2019-12-19 14:26:36');

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
CREATE TABLE IF NOT EXISTS `announcements` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `start_date` timestamp NOT NULL,
  `end_date` timestamp NOT NULL,
  `price` double NOT NULL,
  `points` bigint(20) NOT NULL,
  `views` bigint(20) NOT NULL DEFAULT '0',
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_daily_on` time NOT NULL DEFAULT '12:00:00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `announcements_merchant_id_foreign` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `announcement_wishlists`
--

DROP TABLE IF EXISTS `announcement_wishlists`;
CREATE TABLE IF NOT EXISTS `announcement_wishlists` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `announcement_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `announcement_wishlists_user_id_foreign` (`user_id`),
  KEY `announcement_wishlists_announcement_id_foreign` (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
CREATE TABLE IF NOT EXISTS `areas` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_en` (`name_en`),
  UNIQUE KEY `name_ar` (`name_ar`),
  KEY `areas_city_id_foreign` (`city_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `city_id`, `name_ar`, `name_en`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'شيراتون', 'Sheraton', NULL, NULL, '2019-12-16 14:54:42'),
(2, 2, 'مدينة نصر', 'Nasr City', NULL, NULL, '2019-12-16 14:54:42'),
(3, 3, 'أبو قير', 'Abou Qair', NULL, NULL, NULL),
(4, 3, 'سيدى بشر', 'Sedi Beshr', NULL, NULL, NULL),
(5, 3, 'سيدى جابر', 'Sedi Gaber', NULL, NULL, NULL),
(6, 3, 'المنتزه', 'Al Montazah', NULL, '2019-12-16 15:15:05', '2019-12-16 15:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `area_id` bigint(20) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `branches_name_ar_unique` (`name_ar`),
  UNIQUE KEY `branches_name_en_unique` (`name_en`),
  UNIQUE KEY `branches_email_unique` (`email`),
  KEY `branches_merchant_id_foreign` (`merchant_id`),
  KEY `branches_city_id_foreign` (`city_id`),
  KEY `branches_area_id_foreign` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `merchant_id`, `city_id`, `area_id`, `name_ar`, `name_en`, `email`, `address`, `address_url`, `phone`, `mobile`, `password`, `status`, `blocked`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 3, 3, 3, 'Branch 12', 'Branch 1', 'info@branch1.com', 'asd-asd-asd', 'https://www.google.com/maps/@30.1038885,31.3710165,16z', '021212121212', '01273280587', '$2y$10$3mgpUTf/0Wt2kLj7QxUsZeYCkQLZGyJjM2ix4F1xNG4QFE/35R1Re', 1, 0, NULL, NULL, '2019-12-16 11:35:54', '2019-12-17 13:03:18'),
(2, 1, 2, 1, 'Branch 2', 'Branch 2', 'info@branch2.com', 'asd-asd-asd', 'https://www.google.com/maps/@30.1038885,31.3710165,16z', '021212121212', '01273280587', '$2y$10$khgvbDj9Hcytbt6scaebV.IfXU7cjzC9KnUPPDnVeyUIU4UoJmKr6', 1, 0, NULL, NULL, '2019-12-16 15:02:21', '2019-12-17 13:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `branch_phones`
--

DROP TABLE IF EXISTS `branch_phones`;
CREATE TABLE IF NOT EXISTS `branch_phones` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `branch_phones_branch_id_foreign` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branch_translations`
--

DROP TABLE IF EXISTS `branch_translations`;
CREATE TABLE IF NOT EXISTS `branch_translations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `branch_translations_branch_id_locale_unique` (`branch_id`,`locale`),
  KEY `branch_translations_locale_index` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_ar` (`name_ar`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_ar`, `name_en`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'a', 'Home', NULL, NULL, NULL),
(2, 'b', 'Cafe & bakery', NULL, NULL, NULL),
(3, 'c', 'Restaurants', NULL, NULL, NULL),
(4, 'd', 'Books, Stationary & Media', NULL, NULL, NULL),
(5, 'e', 'Accessories', NULL, NULL, NULL),
(6, 'f', 'Clothing', NULL, NULL, NULL),
(7, 'g', 'Footwear', NULL, NULL, NULL),
(8, 'h', 'Health & Beauty', NULL, NULL, NULL),
(9, 'i', 'Toys & Kids', NULL, NULL, NULL),
(10, 'j', 'Electronics', NULL, NULL, NULL),
(11, 'k', 'Entertainment', NULL, NULL, NULL),
(12, 'أى حاجة', 'Anything', '2019-12-16 13:59:32', '2019-12-16 13:48:43', '2019-12-16 13:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_en` (`name_en`),
  UNIQUE KEY `name_ar` (`name_ar`),
  KEY `countries_parent_id_foreign` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `parent_id`, `name_ar`, `name_en`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, 'مصر', 'Egypt', NULL, NULL, '2019-12-16 12:38:04', '2019-12-16 14:30:16'),
(2, 1, 'القاهرة', 'Cairo', NULL, NULL, '2019-12-16 12:38:04', '2019-12-16 14:54:42'),
(3, 1, 'الإسكندرية', 'Alexandria', NULL, NULL, '2019-12-16 12:38:04', '2019-12-16 12:38:04'),
(4, 1, 'مرسى مطروح', 'Marsa Matrouh', NULL, NULL, '2019-12-16 12:38:04', '2019-12-16 12:38:04'),
(5, 1, 'الغردقة', 'Horghada', NULL, NULL, '2019-12-16 12:38:04', '2019-12-16 12:38:04'),
(6, 1, 'شرم الشيخ', 'Sharm Al-Sheikh', NULL, NULL, '2019-12-16 12:38:04', '2019-12-16 12:38:04'),
(7, 1, 'الجيزة', 'Giza', NULL, NULL, '2019-12-16 14:25:38', '2019-12-16 14:30:05');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `points` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `points`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1000, 1, NULL, '2019-12-25 14:28:41', '2019-12-25 14:38:26'),
(2, 5000, 1, NULL, '2019-12-25 14:30:53', '2019-12-25 14:30:53'),
(3, 5000, 1, NULL, '2019-12-25 14:32:39', '2019-12-25 14:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `event_translations`
--

DROP TABLE IF EXISTS `event_translations`;
CREATE TABLE IF NOT EXISTS `event_translations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `event_translations_event_id_locale_unique` (`event_id`,`locale`),
  KEY `event_translations_locale_index` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `event_translations`
--

INSERT INTO `event_translations` (`id`, `event_id`, `locale`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'عنوان 1', 'وصف', '2019-12-25 14:28:41', '2019-12-25 14:37:53'),
(2, 1, 'en', 'Title', 'Description', '2019-12-25 14:28:41', '2019-12-25 14:36:18'),
(3, 2, 'ar', 'asd-asd-asd', 'asd-asd-asd', '2019-12-25 14:30:53', '2019-12-25 14:30:53'),
(4, 2, 'en', 'asd-asd-asd', 'asd-asd-asd', '2019-12-25 14:30:53', '2019-12-25 14:30:53'),
(5, 3, 'ar', 'asd-asd-asd', 'asd-asd-asd', '2019-12-25 14:32:39', '2019-12-25 14:32:39'),
(6, 3, 'en', 'asd-asd-asd', 'asd-asd-asd', '2019-12-25 14:32:39', '2019-12-25 14:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `event_trigger_types`
--

DROP TABLE IF EXISTS `event_trigger_types`;
CREATE TABLE IF NOT EXISTS `event_trigger_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorite_merchants`
--

DROP TABLE IF EXISTS `favorite_merchants`;
CREATE TABLE IF NOT EXISTS `favorite_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favorite_merchants_merchant_id_foreign` (`merchant_id`),
  KEY `favorite_merchants_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE IF NOT EXISTS `feedback` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `firebase_messages`
--

DROP TABLE IF EXISTS `firebase_messages`;
CREATE TABLE IF NOT EXISTS `firebase_messages` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `firebase_messages_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `like_merchants`
--

DROP TABLE IF EXISTS `like_merchants`;
CREATE TABLE IF NOT EXISTS `like_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `like_merchants_merchant_id_foreign` (`merchant_id`),
  KEY `like_merchants_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `merchants`
--

DROP TABLE IF EXISTS `merchants`;
CREATE TABLE IF NOT EXISTS `merchants` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` bigint(20) NOT NULL DEFAULT '0',
  `max_receipt_value` bigint(20) NOT NULL DEFAULT '1000000000',
  `bio` text COLLATE utf8mb4_unicode_ci,
  `terms` text COLLATE utf8mb4_unicode_ci,
  `enable_points` tinyint(1) NOT NULL DEFAULT '1',
  `enable_vouchers` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `merchants_username_unique` (`username`),
  UNIQUE KEY `name_en` (`name_en`),
  UNIQUE KEY `name_ar` (`name_ar`),
  UNIQUE KEY `merchants_email_unique` (`email`),
  UNIQUE KEY `merchants_phone_unique` (`phone`),
  KEY `merchants_type_id_foreign` (`type_id`),
  KEY `merchants_category_id_foreign` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchants`
--

INSERT INTO `merchants` (`id`, `type_id`, `category_id`, `name_ar`, `name_en`, `email`, `phone`, `username`, `image`, `balance`, `max_receipt_value`, `bio`, `terms`, `enable_points`, `enable_vouchers`, `status`, `blocked`, `password`, `firebase`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'كارفور ', 'Carrefour', 'info@carrefour.com', '01273280587', 'Carrefour', '1576419303pCw75.png', 0, 20000, 'asd-asd-asd', 'asd-asd-asd', 1, 1, 1, 0, '123456', NULL, NULL, NULL, '2019-12-15 09:59:06', '2019-12-15 12:15:03'),
(3, 1, 1, 'كارفور 2', 'Carrefour2', 'info@carrefour2.com', '01273280588', 'Carrefour2', '1576421011rE0LP.png', 0, 20000, 'asd-asd-asd', 'asd-asd-asd', 1, 1, 1, 0, '$2y$10$SMrNX4OndRWgNFWqeKgZ/Oow.jU5LimwZWQ5LgGJTX5Jnns6r98Zu', NULL, NULL, NULL, '2019-12-15 12:35:58', '2019-12-16 12:12:27'),
(4, 1, 1, 'كارفور 3', 'Carrefour 3', 'info@carrefour3.com', '01273280580', 'Carrefour3', '1576517956Ygqtf.jpg', 0, 20000, 'asd-asd-asd', 'asd-asd-asd', 1, 1, 1, 0, '$2y$10$iEAI09wc8zXu7CPnuYVo/uPr7JDttJVVUcUzykY2eoLF/OJZKEWW2', NULL, NULL, NULL, '2019-12-16 15:39:16', '2019-12-16 15:39:16'),
(5, 1, 1, 'كارفور 4', 'Carrefour 4', 'info@carrefour4.com', '01273280581', 'Carrefour4', NULL, 0, 20000, 'asd-asd-asd', 'asd-asd-asd', 1, 1, 1, 0, '$2y$10$SheE22M7bwdu4js0iRRfxuwoUxXxjRIe8appxH7tYh9RtGW77YEQm', NULL, NULL, NULL, '2019-12-16 15:45:18', '2019-12-16 15:45:18'),
(6, 1, 1, 'كارفور 90', 'Carrefour 90', 'info@carrefour90.com', '01273280599', 'Carrefour90', NULL, 0, 20000, 'asd-asd-asd', 'asd-asd-asd', 1, 1, 1, 0, '$2y$10$YvOWJrMtTzNYyzgYkPHC/ORxy69qmsLG1kCg//fjDMO1hx0xesZ1S', NULL, NULL, NULL, '2019-12-25 14:06:20', '2019-12-25 14:06:20'),
(7, NULL, 1, 'كارفور 500', 'Carrefour 500', 'info@carrefour500.com', '01273280500', 'Carrefour500', NULL, 0, 20000, 'asd-asd-asd', 'asd-asd-asd', 1, 1, 1, 0, '$2y$10$RhSgOtRq86KiI6lBaWBCeekI03EcmtxGwO460Xlkw4pIJ2egLk81.', NULL, NULL, NULL, '2019-12-25 14:06:49', '2019-12-25 14:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `merchant_events`
--

DROP TABLE IF EXISTS `merchant_events`;
CREATE TABLE IF NOT EXISTS `merchant_events` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `sponsor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `trigger_type_id` bigint(20) UNSIGNED DEFAULT NULL,
  `rewarded_points` bigint(20) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `start_date` timestamp NOT NULL,
  `end_date` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merchant_events_merchant_id_foreign` (`merchant_id`),
  KEY `merchant_events_sponsor_id_foreign` (`sponsor_id`),
  KEY `merchant_events_trigger_type_id_foreign` (`trigger_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `merchant_media`
--

DROP TABLE IF EXISTS `merchant_media`;
CREATE TABLE IF NOT EXISTS `merchant_media` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('image','video') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'image',
  `media` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merchant_media_merchant_id_foreign` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `merchant_sponsors`
--

DROP TABLE IF EXISTS `merchant_sponsors`;
CREATE TABLE IF NOT EXISTS `merchant_sponsors` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `sponsor_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merchant_sponsors_merchant_id_foreign` (`merchant_id`),
  KEY `merchant_sponsors_sponsor_id_foreign` (`sponsor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merchant_sponsors`
--

INSERT INTO `merchant_sponsors` (`id`, `merchant_id`, `sponsor_id`, `created_at`, `updated_at`) VALUES
(38, 3, 1, NULL, NULL),
(39, 3, 3, NULL, NULL),
(40, 4, 1, NULL, NULL),
(41, 4, 2, NULL, NULL),
(42, 4, 3, NULL, NULL),
(43, 5, 1, NULL, NULL),
(44, 5, 2, NULL, NULL),
(45, 5, 3, NULL, NULL),
(46, 6, 1, NULL, NULL),
(47, 6, 2, NULL, NULL),
(48, 6, 3, NULL, NULL),
(49, 7, 1, NULL, NULL),
(50, 7, 2, NULL, NULL),
(51, 7, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `merchant_translations`
--

DROP TABLE IF EXISTS `merchant_translations`;
CREATE TABLE IF NOT EXISTS `merchant_translations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `merchant_translations_merchant_id_locale_unique` (`merchant_id`,`locale`),
  KEY `merchant_translations_locale_index` (`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_10_111820_create_categories_table', 1),
(5, '2019_12_10_112100_create_sponsors_table', 1),
(6, '2019_12_10_112125_create_types_table', 1),
(8, '2019_12_10_112150_create_areas_table', 1),
(9, '2019_12_10_112205_create_feedback_table', 1),
(10, '2019_12_10_112221_create_abouts_table', 1),
(11, '2019_12_10_112258_create_contacts_table', 1),
(12, '2019_12_10_112319_create_merchants_table', 1),
(15, '2019_12_10_112612_create_favorite_merchants_table', 1),
(16, '2019_12_10_112659_create_like_merchants_table', 1),
(17, '2019_12_10_112732_create_rate_merchants_table', 1),
(18, '2019_12_10_112751_create_firebase_messages_table', 1),
(20, '2019_12_10_112858_create_merchant_media_table', 1),
(21, '2019_12_10_112913_create_announcements_table', 1),
(22, '2019_12_10_112931_create_used_announcements_table', 1),
(23, '2019_12_10_113018_create_announcement_wishlists_table', 1),
(24, '2019_12_10_113034_create_user_locations_table', 1),
(25, '2019_12_10_113103_create_merchant_sponsors_table', 1),
(26, '2019_12_10_113211_create_point_maps_table', 1),
(27, '2019_12_10_113227_create_user_devices_table', 1),
(29, '2019_12_10_113310_create_merchant_events_table', 1),
(32, '2019_12_10_113744_create_branch_vouchers_table', 1),
(35, '2019_12_15_113100_create_voucher_prices_table', 1),
(38, '2019_12_10_112141_create_countries_table', 3),
(39, '2019_12_10_112449_create_branches_table', 4),
(40, '2019_12_10_112842_create_branch_phones_table', 4),
(41, '2019_12_17_121427_create_voucher_translations_table', 5),
(42, '2019_12_17_121540_create_merchant_translations_table', 5),
(43, '2019_12_17_121554_create_branch_translations_table', 5),
(44, '2019_12_10_112528_create_vouchers_table', 6),
(45, '2019_12_17_130551_create_voucher_branches_table', 7),
(46, '2019_12_10_114054_create_admins_table', 8),
(48, '2014_10_12_000000_create_users_table', 9),
(49, '2019_12_10_113701_create_user_vouchers_table', 9),
(50, '2019_12_24_125230_create_user_points_table', 9),
(51, '2019_12_25_155325_create_events_table', 10),
(52, '2019_12_25_155431_create_event_translations_table', 10),
(53, '2019_12_25_155752_create_user_events_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `point_maps`
--

DROP TABLE IF EXISTS `point_maps`;
CREATE TABLE IF NOT EXISTS `point_maps` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `spend` double NOT NULL,
  `reward` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `point_maps_merchant_id_foreign` (`merchant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rate_merchants`
--

DROP TABLE IF EXISTS `rate_merchants`;
CREATE TABLE IF NOT EXISTS `rate_merchants` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `rate` double(8,2) NOT NULL DEFAULT '0.00',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rate_merchants_merchant_id_foreign` (`merchant_id`),
  KEY `rate_merchants_branch_id_foreign` (`branch_id`),
  KEY `rate_merchants_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE IF NOT EXISTS `sponsors` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `name`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Nabda', 1, NULL, NULL, NULL),
(2, 'Balsamee', 1, NULL, NULL, NULL),
(3, 'BeWell', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('admin','merchant') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `type`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'merchant', 'Merchant', '', NULL, NULL),
(2, 'merchant', 'Charity', '', NULL, NULL),
(3, 'merchant', 'Virtual', '', NULL, NULL),
(4, 'admin', 'admin', '', NULL, NULL),
(5, 'admin', 'super admin', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `used_announcements`
--

DROP TABLE IF EXISTS `used_announcements`;
CREATE TABLE IF NOT EXISTS `used_announcements` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `announcement_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `used_announcements_user_id_foreign` (`user_id`),
  KEY `used_announcements_announcement_id_foreign` (`announcement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sponsor_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firebase` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT '2019-12-24 12:03:59',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_sponsor_id_foreign` (`sponsor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `sponsor_id`, `name`, `email`, `phone`, `username`, `image`, `email_verified_at`, `password`, `firebase`, `status`, `blocked`, `gender`, `deleted_at`, `last_login`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tarek', 'tarek4@oencil-designs.com', '01111111114', 'tarek4', NULL, NULL, '$2y$10$oVMsoqQye2yZNiGrdyHy8u/634XnYoMqozsYVJCYXbHUcgw0kMMSO', NULL, 1, 0, 'male', NULL, '2019-12-24 12:03:59', NULL, '2019-12-24 12:07:01', '2019-12-24 12:07:01'),
(2, 1, 'Tarek', 'tarek@oencil-designs.com', '01111111111', 'tarek1', NULL, NULL, '$2y$10$oVMsoqQye2yZNiGrdyHy8u/634XnYoMqozsYVJCYXbHUcgw0kMMSO', NULL, 1, 0, 'male', NULL, '2019-12-24 12:03:59', NULL, '2019-12-24 12:07:01', '2019-12-24 12:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `user_devices`
--

DROP TABLE IF EXISTS `user_devices`;
CREATE TABLE IF NOT EXISTS `user_devices` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_devices_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_events`
--

DROP TABLE IF EXISTS `user_events`;
CREATE TABLE IF NOT EXISTS `user_events` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_events_user_id_foreign` (`user_id`),
  KEY `user_events_event_id_foreign` (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_events`
--

INSERT INTO `user_events` (`id`, `event_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-12-25 15:47:45', '2019-12-25 15:47:45'),
(2, 1, 1, '2019-12-25 15:51:45', '2019-12-25 15:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_locations`
--

DROP TABLE IF EXISTS `user_locations`;
CREATE TABLE IF NOT EXISTS `user_locations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_locations_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_points`
--

DROP TABLE IF EXISTS `user_points`;
CREATE TABLE IF NOT EXISTS `user_points` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `points` bigint(20) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_points_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_points`
--

INSERT INTO `user_points` (`id`, `user_id`, `points`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'Initial Action', '2019-12-24 12:07:01', '2019-12-24 12:07:01'),
(2, 1, 1000, 'Points added from event: Title', '2019-12-25 15:51:45', '2019-12-25 15:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `user_vouchers`
--

DROP TABLE IF EXISTS `user_vouchers`;
CREATE TABLE IF NOT EXISTS `user_vouchers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `voucher_id` bigint(20) UNSIGNED NOT NULL,
  `redeemed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_vouchers_user_id_foreign` (`user_id`),
  KEY `user_vouchers_voucher_id_foreign` (`voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_vouchers`
--

INSERT INTO `user_vouchers` (`id`, `user_id`, `voucher_id`, `redeemed`, `created_at`, `updated_at`) VALUES
(1, 2, 20, 1, NULL, '2019-12-24 13:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE IF NOT EXISTS `vouchers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cash_cost` double NOT NULL,
  `pin_code` int(4) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `start_at` timestamp NOT NULL DEFAULT '2019-12-17 10:39:45',
  `end_at` timestamp NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vouchers_merchant_id_foreign` (`merchant_id`),
  KEY `vouchers_category_id_foreign` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `merchant_id`, `category_id`, `image`, `cash_cost`, `pin_code`, `status`, `start_at`, `end_at`, `deleted_at`, `created_at`, `updated_at`) VALUES
(20, 3, 2, '1576598128fFCZi.png', 2000, 1234, 1, '2019-12-16 22:00:00', '2019-12-23 22:00:00', NULL, '2019-12-17 13:55:28', '2019-12-17 14:27:21'),
(21, 1, 1, '1576598128fFCZi.png', 2000, 1234, 1, '2019-12-16 22:00:00', '2019-12-23 22:00:00', NULL, '2019-12-17 13:55:28', '2019-12-17 14:27:21');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_branches`
--

DROP TABLE IF EXISTS `voucher_branches`;
CREATE TABLE IF NOT EXISTS `voucher_branches` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `branch_id` bigint(20) UNSIGNED NOT NULL,
  `voucher_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voucher_branches_branch_id_foreign` (`branch_id`),
  KEY `voucher_branches_voucher_id_foreign` (`voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucher_branches`
--

INSERT INTO `voucher_branches` (`id`, `branch_id`, `voucher_id`, `created_at`, `updated_at`) VALUES
(73, 1, 20, NULL, NULL),
(74, 2, 21, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `voucher_prices`
--

DROP TABLE IF EXISTS `voucher_prices`;
CREATE TABLE IF NOT EXISTS `voucher_prices` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voucher_id` bigint(20) UNSIGNED NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `voucher_prices_voucher_id_foreign` (`voucher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucher_prices`
--

INSERT INTO `voucher_prices` (`id`, `voucher_id`, `price`, `created_at`, `updated_at`) VALUES
(109, 20, 1000, NULL, NULL),
(110, 20, 2000, NULL, NULL),
(111, 20, 3000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `voucher_translations`
--

DROP TABLE IF EXISTS `voucher_translations`;
CREATE TABLE IF NOT EXISTS `voucher_translations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `voucher_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voucher_translations_voucher_id_locale_unique` (`voucher_id`,`locale`),
  KEY `voucher_translations_locale_index` (`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucher_translations`
--

INSERT INTO `voucher_translations` (`id`, `voucher_id`, `locale`, `title`, `description`, `created_at`, `updated_at`) VALUES
(31, 20, 'ar', 'عنوان', 'وصف', '2019-12-17 13:55:28', '2019-12-17 13:55:41'),
(32, 20, 'en', 'Title', 'Description', '2019-12-17 13:55:28', '2019-12-17 13:55:41');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `announcements`
--
ALTER TABLE `announcements`
  ADD CONSTRAINT `announcements_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `announcement_wishlists`
--
ALTER TABLE `announcement_wishlists`
  ADD CONSTRAINT `announcement_wishlists_announcement_id_foreign` FOREIGN KEY (`announcement_id`) REFERENCES `announcements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `announcement_wishlists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_area_id_foreign` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branches_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branches_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branch_phones`
--
ALTER TABLE `branch_phones`
  ADD CONSTRAINT `branch_phones_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branch_translations`
--
ALTER TABLE `branch_translations`
  ADD CONSTRAINT `branch_translations_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `countries`
--
ALTER TABLE `countries`
  ADD CONSTRAINT `countries_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `countries` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `event_translations`
--
ALTER TABLE `event_translations`
  ADD CONSTRAINT `event_translations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorite_merchants`
--
ALTER TABLE `favorite_merchants`
  ADD CONSTRAINT `favorite_merchants_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorite_merchants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `firebase_messages`
--
ALTER TABLE `firebase_messages`
  ADD CONSTRAINT `firebase_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `like_merchants`
--
ALTER TABLE `like_merchants`
  ADD CONSTRAINT `like_merchants_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `like_merchants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `merchants`
--
ALTER TABLE `merchants`
  ADD CONSTRAINT `merchants_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `merchants_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `merchant_events`
--
ALTER TABLE `merchant_events`
  ADD CONSTRAINT `merchant_events_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `merchant_events_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `merchant_events_trigger_type_id_foreign` FOREIGN KEY (`trigger_type_id`) REFERENCES `event_trigger_types` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `merchant_media`
--
ALTER TABLE `merchant_media`
  ADD CONSTRAINT `merchant_media_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `merchant_sponsors`
--
ALTER TABLE `merchant_sponsors`
  ADD CONSTRAINT `merchant_sponsors_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `merchant_sponsors_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `merchant_translations`
--
ALTER TABLE `merchant_translations`
  ADD CONSTRAINT `merchant_translations_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `point_maps`
--
ALTER TABLE `point_maps`
  ADD CONSTRAINT `point_maps_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `rate_merchants`
--
ALTER TABLE `rate_merchants`
  ADD CONSTRAINT `rate_merchants_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `rate_merchants_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `rate_merchants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `used_announcements`
--
ALTER TABLE `used_announcements`
  ADD CONSTRAINT `used_announcements_announcement_id_foreign` FOREIGN KEY (`announcement_id`) REFERENCES `announcements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `used_announcements_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_sponsor_id_foreign` FOREIGN KEY (`sponsor_id`) REFERENCES `sponsors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_devices`
--
ALTER TABLE `user_devices`
  ADD CONSTRAINT `user_devices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_events`
--
ALTER TABLE `user_events`
  ADD CONSTRAINT `user_events_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_events_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_locations`
--
ALTER TABLE `user_locations`
  ADD CONSTRAINT `user_locations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_points`
--
ALTER TABLE `user_points`
  ADD CONSTRAINT `user_points_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_vouchers`
--
ALTER TABLE `user_vouchers`
  ADD CONSTRAINT `user_vouchers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_vouchers_voucher_id_foreign` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD CONSTRAINT `vouchers_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `vouchers_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `voucher_branches`
--
ALTER TABLE `voucher_branches`
  ADD CONSTRAINT `voucher_branches_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `voucher_branches_voucher_id_foreign` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `voucher_prices`
--
ALTER TABLE `voucher_prices`
  ADD CONSTRAINT `voucher_prices_voucher_id_foreign` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `voucher_translations`
--
ALTER TABLE `voucher_translations`
  ADD CONSTRAINT `voucher_translations_voucher_id_foreign` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
