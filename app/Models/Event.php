<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Event extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','deleted_at','name','description','translations','pivot'];
    protected $appends = ['name_ar','name_en','description_ar','description_en', ];
    public $translatedAttributes = ['name', 'description'];

    public function getNameArAttribute()
    {
        return $this->translateOrNew('ar')->name;
    }
    public function getNameEnAttribute()
    {
        return $this->translateOrNew('en')->name;
    }
    public function getDescriptionArAttribute()
    {
        return $this->translateOrNew('ar')->description;
    }
    public function getDescriptionEnAttribute()
    {
        return $this->translateOrNew('en')->description;
    }
}
