<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use URL;

class Branch extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    protected $guarded = [];
    protected $appends = ['image_path'];
    protected $hidden = ['password', 'remember_token','created_at','updated_at','image','blocked','deleted_at','pivot'];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($input)
    {
        if($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }
    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_URL').'/images/merchants/'.$this->image : null;
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id')->withDefault();
    }
    public function city()
    {
        return $this->belongsTo(Country::class, 'city_id')->withDefault();
    }
    public function area()
    {
        return $this->belongsTo(Area::class, 'area_id')->withDefault();
    }
    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class, 'voucher_branches','branch_id','voucher_id');
    }


    #  JWT ==================================
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

}
