<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use URL;

class Merchant extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    #protected $table = 'admins';
    protected $guarded = [];
    protected $appends = ['logo_path'];
    protected $hidden = ['password', 'remember_token','created_at','updated_at','logo','email_verified_at','blocked','deleted_at'];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($input)
    {
        if($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function getLogoPathAttribute()
    {
        return $this->logo ? env('APP_URL').'/images/merchants/'.$this->logo : null;
    }

    public function branches()
    {
        return $this->hasMany(Branch::class, 'merchant_id')->where('status',1);
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->withDefault();
    }
    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id')->withDefault();
    }
    public function sponsors()
    {
        return $this->belongsToMany(Sponsor::class, 'merchant_sponsors','merchant_id','sponsor_id');
    }


    #  JWT ==================================
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }


    #  ==================================
    public static function boot()
    {
        parent::boot();
    }
}
