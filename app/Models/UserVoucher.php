<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVoucher extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at',];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }
    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id')->withDefault();
    }
}
