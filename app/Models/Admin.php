<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use URL;

class Admin extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    #protected $table = 'admins';
    protected $guarded = [];
    protected $appends = ['image_path','type_name'];
    protected $hidden = ['type','password', 'remember_token','created_at','updated_at','image','deleted_at',];

    public function setPasswordAttribute($input)
    {
        if($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_URL').'/images/users/'.$this->image : null;
    }
    public function getTypeNameAttribute()
    {
        return $this->type->name;
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id')->withDefault();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
}
