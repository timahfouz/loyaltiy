<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','deleted_at','parent_id','image',];

    public function areas()
    {
        return $this->hasMany(Area::class, 'city_id');
    }


    public static function boot()
    {
        parent::boot();

        static::creating(function($city) {
            $city->parent_id = 1;
        });

        static::deleting(function($city) {
            $city->areas()->delete();
        });
    }

}
