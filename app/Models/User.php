<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use URL;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    #protected $table = 'admins';
    protected $guarded = [];
    protected $appends = ['image_path','points'];
    protected $hidden = [
        'password', 'remember_token','updated_at','image',
        'email_verified_at','deleted_at','last_login'
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($input)
    {
        if($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function getImagePathAttribute()
    {
        return $this->image ? env('APP_URL').'/images/users/'.$this->image : null;
    }

    public function getPointsAttribute()
    {
        return array_sum($this->transactions->pluck('points')->toArray());
    }

    public function sponsor()
    {
        return $this->belongsTo(Sponsor::class, 'sponsor_id')->withDefault();
    }

    public function transactions()
    {
        return $this->hasMany(UserPoint::class, 'user_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function boot()
    {
        parent::boot();
        static::created(function($user){
            UserPoint::create([
                'user_id' => $user->id,
                'points' => 0,
                'description' => 'Initial Action',
            ]);
        });
    }

}
