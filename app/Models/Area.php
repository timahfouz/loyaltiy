<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Area extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','deleted_at',];

    public function city()
    {
        return $this->belongsTo(Country::class, 'city_id')->withDefault();
    }
}
