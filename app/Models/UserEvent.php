<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserEvent extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at',];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }
    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id')->withDefault();
    }
}
