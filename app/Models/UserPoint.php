<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPoint extends Model
{
    protected $guarded = [];
    protected $casts = [
        'created_at' => 'datetime:d, F Y h:ia'
    ];
    protected $hidden = ['id','user_id','updated_at',];


}
