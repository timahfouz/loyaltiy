<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherBranch extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','pivot',];

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id')->withDefault();
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id')->withDefault();
    }
}
