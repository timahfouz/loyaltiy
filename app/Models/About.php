<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $guarded = [];
    protected $appends = ['image_path'];
    protected $hidden = ['created_at','updated_at',];
}
