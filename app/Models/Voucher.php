<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Voucher extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','deleted_at','title','description','translations','pivot'];
    protected $appends = ['expired','title_ar','title_en','description_ar','description_en', ];
    public $translatedAttributes = ['title', 'description'];

    public function getExpiredAttribute()
    {
        return $this->end_at >= \Carbon\Carbon::today() ? false : true;
    }
    public function getTitleArAttribute()
    {
        return $this->translateOrNew('ar')->title;
    }
    public function getTitleEnAttribute()
    {
        return $this->translateOrNew('en')->title;
    }
    public function getDescriptionArAttribute()
    {
        return $this->translateOrNew('ar')->description;
    }
    public function getDescriptionEnAttribute()
    {
        return $this->translateOrNew('en')->description;
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function prices()
    {
        return $this->hasMany(VoucherPrice::class, 'voucher_id');
    }
    public function voucherBranches()
    {
        return $this->hasMany(VoucherBranch::class, 'voucher_id');
    }
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'voucher_branches','voucher_id','branch_id');
    }
}
