<?php

namespace App\Http\Middleware;

use Closure;

class ActiveAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = \Auth::guard('admin_api')->user();
        if(!$admin || !$admin->status) {
            if($request->expectsJson()) {
                return response()->json([
                    'code' => 401,
                    'message' => 'You\'ve to activate your account.',
                    'data' => [],
                ],401);
            }
        }
        return $next($request);
    }
}
