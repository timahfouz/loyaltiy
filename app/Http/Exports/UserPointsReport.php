<?php

namespace App\Http\Exports;

use App\Http\Services\CommonService;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Http\Request;

class UserPointsReport implements FromView,ShouldAutoSize
{
    private $status;
    private $serviceObj;
    public function __construct(Request $request = null)
    {
        $this->request = $request;
        $this->serviceObj = new CommonService();
    }

    public function view(): View
    {
        $request = $this->request;
        $conditions = [];
        if($request->filled('status'))
            $conditions['status'] = $request->status;

        $items = $this->serviceObj->getAll('User',$conditions,['transactions'], 100);

        $items->map(function($item){
            $item['id'] = $item->id;
            $item['network'] = $item->sponsor->name;
            $item['available_points'] = array_sum($this->serviceObj->getAll('UserPoint',['user_id' => $item->id])->pluck('points')->toArray());
            $item['total_points_over_the_time'] = array_sum($this->serviceObj->getAll('UserPoint',[['points','>',0],'user_id' => $item->id])->pluck('points')->toArray());
            $item['spent_points_over_the_time'] = array_sum($this->serviceObj->getAll('UserPoint',[['points','<',0],'user_id' => $item->id])->pluck('points')->toArray());
        });
        return view('Admin.reports.user-points', [
            'items' => $items
        ]);
    }
}
