<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class MerchantRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    $rules = [
                        'type_id' => 'nullable|exists:types,id',
                        'category_id' => 'required|exists:categories,id',
                        'name_ar' => 'required|min:2|unique:merchants,name_ar',
                        'name_en' => 'required|min:2|unique:merchants,name_en',
                        'email' => 'required|email|unique:merchants,email',
                        'phone' => 'required|digits_between:9,14|unique:merchants,phone',
                        'address' => 'nullable|max:60',
                        'username' => 'required|unique:merchants,username',
                        'max_receipt_value' => 'required|numeric',
                        'enable_points' => 'required|numeric|boolean',
                        'enable_vouchers' => 'required|numeric|boolean',
                        'password' => 'required|min:4|confirmed',
                        'image' => 'image|mimes:jpeg,jpg,png|max:2048',
                    ];
                    if (!empty($this->request->get('sponsors'))) {
                        foreach($this->request->get('sponsors') as $key => $val) {
                            $rules['sponsors.'.$key] = 'required|exists:sponsors,id';
                        }
                    }
                    return $rules;
                }
            case 'PUT':
                {
                    $rules = [
                        'type_id' => 'nullable|exists:types,id',
                        'category_id' => 'nullable|exists:categories,id',
                        'name_ar' => 'nullable|min:2|unique:merchants,name_ar,'.$this->segment(3),
                        'name_en' => 'nullable|min:2|unique:merchants,name_en,'.$this->segment(3),
                        'email' => 'nullable|email|unique:merchants,email,'.$this->segment(3),
                        'phone' => 'nullable|digits_between:9,14|unique:merchants,phone,'.$this->segment(3),
                        'address' => 'nullable|max:60',
                        'username' => 'nullable|unique:merchants,username,'.$this->segment(3),
                        'max_receipt_value' => 'nullable|numeric',
                        'enable_points' => 'nullable|numeric|boolean',
                        'enable_vouchers' => 'nullable|numeric|boolean',
                        'password' => 'nullable|min:4|confirmed',
                        'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
                    ];
                    if (!empty($this->request->get('sponsors'))) {
                        foreach($this->request->get('sponsors') as $key => $val) {
                            $rules['sponsors.'.$key] = 'required|exists:sponsors,id';
                        }
                    }
                    return $rules;
                    // if(request()->wantsJson()) {
                    //     return [
                    //         'type_id' => 'nullable|exists:types,id',
                    //         'category_id' => 'nullable|exists:categories,id',
                    //         'name' => 'nullable|min:2',
                    //         'email' => 'nullable|email|unique:merchants,email,'.$this->user()->id,
                    //         'phone' => 'nullable|digits_between:9,14|unique:merchants,phone,'.$this->user()->id,
                    //         'username' => 'nullable|unique:merchants,username,'.$this->user()->id,
                    //         'max_receipt_value' => 'nullable|numeric',
                    //         'enable_points' => 'nullable|numeric|boolean',
                    //         'enable_vouchers' => 'nullable|numeric|boolean',
                    //         'password' => 'nullable|min:4|confirmed',
                    //         'image' => 'nullable|image',
                    //     ];
                    // } else {
                    //     return [
                    //         'type_id' => 'nullable|exists:types,id',
                    //         'category_id' => 'nullable|exists:categories,id',
                    //         'name' => 'nullable|min:2',
                    //         'email' => 'nullable|email|unique:merchants,email,'.$this->segment(3),
                    //         'phone' => 'nullable|digits_between:9,14|unique:merchants,phone,'.$this->segment(3),
                    //         'username' => 'nullable|unique:merchants,username,'.$this->segment(3),
                    //         'max_receipt_value' => 'nullable|numeric',
                    //         'enable_points' => 'nullable|numeric|boolean',
                    //         'enable_vouchers' => 'nullable|numeric|boolean',
                    //         'password' => 'nullable|min:4|confirmed',
                    //         'image' => 'nullable|image',
                    //     ];
                    // }

                }
            default:
                break;
        }
    }
}