<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class CategoryRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'name_ar' => 'required|min:2|max:60|unique:categories,name_ar',
                        'name_en' => 'required|min:2|max:60|unique:categories,name_en',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'name_ar' => 'nullable|min:2|max:60|unique:categories,name_ar,'.$this->segment(3),
                        'name_en' => 'nullable|min:2|max:60|unique:categories,name_en,'.$this->segment(3),
                    ];
                }
            default:
                break;
        }
    }
}