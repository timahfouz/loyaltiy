<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class VoucherRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    $rules = [
                        'merchant_id' => 'required|exists:merchants,id',
                        'category_id' => 'required|exists:categories,id',
                        'title.ar' => 'required|min:2|max:60',
                        'title.en' => 'required|min:2|max:60',
                        'description.ar' => 'required|min:2|max:250',
                        'description.en' => 'required|min:2|max:250',
                        'cash_cost' => 'required|digits_between:1,6',
                        'start_at' => 'nullable|date|date_format:Y-m-d|after:yesterday|before:end_at',
                        'end_at' => 'required|date|date_format:Y-m-d|after:start_at|after:today',
                        'branches' => 'required|present|array',
                        'prices' => 'required|present|array|size:3',
                        'pin_code' => 'required|digits:4',
                        'image' => 'required|image|mimes:jpeg,jpg,png|max:2048',
                    ];
                    if (!empty($this->request->get('branches'))) {
                        foreach($this->request->get('branches') as $key => $val) {
                            $rules['branches.'.$key] = 'required|exists:branches,id';
                        }
                    }
                    if (!empty($this->request->get('prices'))) {
                        foreach($this->request->get('prices') as $key => $val) {
                            $rules['prices.'.$key] = 'required|digits_between:1,6';
                        }
                    }
                    return $rules;
                }
            case 'PUT':
                {
                    $rules = [
                        'merchant_id' => 'nullable|exists:merchants,id',
                        'category_id' => 'nullable|exists:categories,id',
                        'name_ar' => 'nullable|min:2|max:60',
                        'name_en' => 'nullable|min:2|max:60',
                        'cash_cost' => 'nullable|digits_between:1,6',
                        'start_at' => 'nullable|date|date_format:Y-m-d|after:yesterday|before:end_at',
                        'end_at' => 'nullable|date|date_format:Y-m-d|after:start_at',
                        'branches' => 'nullable|array',
                        'prices' => 'nullable|array|size:3',
                        'pin_code' => 'nullable|digits:4',
                        'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
                    ];
                    if (!empty($this->request->get('branches'))) {
                        foreach($this->request->get('branches') as $key => $val) {
                            $rules['branches.'.$key] = 'required|exists:branches,id';
                        }
                    }
                    if (!empty($this->request->get('prices'))) {
                        foreach($this->request->get('prices') as $key => $val) {
                            $rules['prices.'.$key] = 'required|digits_between:1,6';
                        }
                    }
                    return $rules;
                }
            default:
                break;
        }
    }
}