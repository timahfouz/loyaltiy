<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class BranchRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    $rules = [
                        'merchant_id' => 'required|exists:merchants,id',
                        'city_id' => 'required|exists:countries,id',
                        'area_id' => 'required|exists:areas,id',
                        'name_ar' => 'required|min:2|unique:branches,name_ar',
                        'name_en' => 'required|min:2|unique:branches,name_en',
                        'email' => 'required|email|unique:branches,email',
                        'address' => 'nullable|max:50',
                        'address_url' => 'required|url',
                        'phone' => 'digits:12',
                        'mobile' => 'required|digits:11',
                        'password' => 'required|min:6|confirmed',
                        'image' => 'nullable|image',
                    ];
                    return $rules;
                }
            case 'PUT':
                {
                    $rules = [
                        'merchant_id' => 'nullable|exists:merchants,id',
                        'city_id' => 'nullable|exists:countries,id',
                        'area_id' => 'nullable|exists:areas,id',
                        'name_ar' => 'nullable|min:2|unique:branches,name_ar,'.$this->segment(3),
                        'name_en' => 'nullable|min:2|unique:branches,name_en,'.$this->segment(3),
                        'email' => 'nullable|email|unique:branches,email,'.$this->segment(3),
                        'address' => 'nullable|max:50',
                        'address_url' => 'required|url',
                        'phone' => 'digits:12',
                        'mobile' => 'nullable|digits:11',
                        'password' => 'nullable|min:6|confirmed',
                        'image' => 'nullable|image',
                    ];
                    return $rules;
                    // if(request()->wantsJson()) {
                    //     return [
                    //         'type_id' => 'nullable|exists:types,id',
                    //         'category_id' => 'nullable|exists:categories,id',
                    //         'name' => 'nullable|min:2',
                    //         'email' => 'nullable|email|unique:merchants,email,'.$this->user()->id,
                    //         'phone' => 'nullable|digits_between:9,14|unique:merchants,phone,'.$this->user()->id,
                    //         'username' => 'nullable|unique:merchants,username,'.$this->user()->id,
                    //         'max_receipt_value' => 'nullable|numeric',
                    //         'enable_points' => 'nullable|numeric|boolean',
                    //         'enable_vouchers' => 'nullable|numeric|boolean',
                    //         'password' => 'nullable|min:4|confirmed',
                    //         'image' => 'nullable|image',
                    //     ];
                    // } else {
                    //     return [
                    //         'type_id' => 'nullable|exists:types,id',
                    //         'category_id' => 'nullable|exists:categories,id',
                    //         'name' => 'nullable|min:2',
                    //         'email' => 'nullable|email|unique:merchants,email,'.$this->segment(3),
                    //         'phone' => 'nullable|digits_between:9,14|unique:merchants,phone,'.$this->segment(3),
                    //         'username' => 'nullable|unique:merchants,username,'.$this->segment(3),
                    //         'max_receipt_value' => 'nullable|numeric',
                    //         'enable_points' => 'nullable|numeric|boolean',
                    //         'enable_vouchers' => 'nullable|numeric|boolean',
                    //         'password' => 'nullable|min:4|confirmed',
                    //         'image' => 'nullable|image',
                    //     ];
                    // }

                }
            default:
                break;
        }
    }
}