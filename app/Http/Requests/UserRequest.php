<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class UserRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'sponsor_id' => 'required|exists:sponsors,id',
                        'name' => 'required|min:2|max:50',
                        'email' => 'required|email|unique:users,email',
                        'username' => 'required|unique:users,username',
                        'phone' => 'nullable|digits_between:9,14|unique:users,phone',
                        'password' => 'nullable|min:4|confirmed',
                        'image' => 'image|mimes:jpeg,jpg,png|max:2048',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'sponsor_id' => 'nullable|exists:sponsors,id',
                        'name' => 'nullable|min:2|max:50',
                        'email' => 'nullable|email|unique:users,email,'.$this->segment(3),
                        'username' => 'nullable|unique:users,username,'.$this->segment(3),
                        'phone' => 'nullable|digits_between:9,14|unique:users,phone,'.$this->segment(3),
                        'password' => 'nullable|min:4|confirmed',
                        'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
                    ];

                }
            default:
                break;
        }
    }
}