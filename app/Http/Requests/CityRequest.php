<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class CityRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'parent_id' => 'nullable|exists:countries,id',
                        'name_ar' => 'required|min:2|max:60|unique:countries,name_ar',
                        'name_en' => 'required|min:2|max:60|unique:countries,name_en',
                        'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'parent_id' => 'nullable|exists:countries,id',
                        'name_ar' => 'nullable|min:2|max:60|unique:countries,name_ar,'.$this->segment(3),
                        'name_en' => 'nullable|min:2|max:60|unique:countries,name_en,'.$this->segment(3),
                        'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
                    ];
                }
            default:
                break;
        }
    }
}