<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class EventRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'points' => 'required|digits_between:1,5',
                        'name.ar' => 'required|min:2|max:60',
                        'name.en' => 'required|min:2|max:60',
                        'description.ar' => 'required|min:2|max:250',
                        'description.en' => 'required|min:2|max:250',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'points' => 'nullable|digits_between:1,5',
                        'name.ar' => 'nullable|min:2|max:60',
                        'name.en' => 'nullable|min:2|max:60',
                        'description.ar' => 'nullable|min:2|max:250',
                        'description.en' => 'nullable|min:2|max:250',
                    ];
                }
            default:
                break;
        }
    }
}