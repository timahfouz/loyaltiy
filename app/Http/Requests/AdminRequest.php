<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class AdminRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'type_id' => 'required|exists:types,id',
                        'name' => 'required|min:2|max:50',
                        'email' => 'required|email|unique:admins,email',
                        'phone' => 'nullable|digits_between:9,14|unique:admins,phone',
                        'password' => 'required|min:4|confirmed',
                        'image' => 'image|mimes:jpeg,jpg,png|max:2048',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'type_id' => 'nullable|exists:types,id',
                        'name' => 'required|min:2|max:50',
                        'email' => 'nullable|email|unique:admins,email,'.$this->segment(3),
                        'phone' => 'nullable|digits_between:9,14|unique:admins,phone,'.$this->segment(3),
                        'password' => 'nullable|min:4|confirmed',
                        'image' => 'nullable|image|mimes:jpeg,jpg,png|max:2048',
                    ];

                }
            default:
                break;
        }
    }
}