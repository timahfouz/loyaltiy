<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class UserEventRequest extends ResponseShape
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return [
                        'user_id' => 'required|exists:users,id',
                        'event_id' => 'required|exists:events,id',
                    ];
                }
            case 'PUT':
                {
                    return [
                        'user_id' => 'required|exists:users,id',
                        'event_id' => 'required|exists:events,id',
                    ];

                }
            default:
                break;
        }
    }
}