<?php

namespace App\Http\Services;

/**
 *  Tarek Mahfouz
 */

class CommonService
{
    private $model;
    private static $namespace = 'App\Models\\';

    public function getAll($model ,$conditions = [], $with = [], $per_page = 0, $whereIn = [], $reverse = true, $multiKeysSearch = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $items = count($with) ? $this->model->with($with) : $this->model;
        $items = $items->where($conditions);
        $items = (count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) ? $items->whereIn($whereIn['key'], $whereIn['values']) : $items;
        if(count($multiKeysSearch)) {
            $items = $items->where(function($sql) use($multiKeysSearch) {
                if($multiKeysSearch['fields'][0]['like'])
                    $sql->where($multiKeysSearch['fields'][0]['field'],'LIKE','%'.$multiKeysSearch['key'].'%');
                else
                    $sql->where($multiKeysSearch['fields'][0]['field'], $multiKeysSearch['key']);
                foreach($multiKeysSearch['fields'] as $field) {
                    $sql->orWhere($field['field'], $multiKeysSearch['key']);
                    if($field['like'])
                        $sql->orWhere($field['field'],'LIKE','%'.$multiKeysSearch['key'].'%');
                    else
                        $sql->orWhere($field['field'], $multiKeysSearch['key']);
                }
            });
        }
        $items = $reverse ?
            (
            $per_page ?
                $items->orderBy('id','DESC')->paginate($per_page) :
                $items->orderBy('id','DESC')->get()
            )
            :
            (
            $per_page ?
                $items->paginate($per_page) :
                $items->get()
            );
        return $items;
    }

    public function find($model ,$conditions = [], $with = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = count($with) ? $this->model->with($with) : $this->model;
        $item = $item->where($conditions)->first();

        return $item;
    }

    public function getOne($model ,$conditions = [], $with = [], $last = false)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = count($with) ? $this->model->with($with) : $this->model;
        $item = $last ? $item->where($conditions)->orderBy('id','DESC')->first() : $item->where($conditions)->first();
        return $item;
    }

    public function create($model ,array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->create($data);
        return $item;
    }

    public function bulkInsert($model ,array $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $item = $this->model->insert($data);
        return $item;
    }

    public function update($model ,$condition, $data)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        $this->model->where($condition)->update($data);
        $item = $this->model->where($condition)->first();
        return $item;
    }

    public function saveTranslation($model ,$data = [], $translationData = [], $add = true)
    {
        $class = self::$namespace.$model;
        $this->model = new $class();

        if(!$add) {
            $item = $this->model->where($data)->first();
        } else {
            $item = $this->model->create($data);
        }
        foreach($translationData as $td) {
            $item->translateOrNew($td['lang'])[$td['field']] = $td['value'];
            $item->save();
        }
        return $item;
    }

    public function destroy($model ,$condition = [], $whereIn = [], $relations = [])
    {
        $class = self::$namespace.$model;
        $this->model = new $class();
        $items = $this->model;
        if((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && !count($condition))
            $items = $this->model->whereIn($whereIn['key'], $whereIn['values']);
        elseif(!count($whereIn) && count($condition))
            $items = $this->model->where($condition);
        elseif((count($whereIn) && isset($whereIn['key']) && isset($whereIn['values'])) && count($condition))
            $items = $this->model->where($condition)->whereIn($whereIn['key'], $whereIn['values']);
        //$this->model->where($condition)->whereIn($key, $whereIn)->delete();
        $items = $items->get();

        foreach($items as $item) {
            $item->delete();
        }
        return true;
    }


}