<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Services\CommonService;
use Illuminate\Http\Request;

class InitController extends Controller
{
    public $serviceObj;
    public function __construct()
    {
        $this->serviceObj = new CommonService();
    }
}
