<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    # ============ POST METHODS ================
    public function storeUser(UserRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['image', 'password_confirmation']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users');
            }
            $result = $this->serviceObj->create('User', $data);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
}
