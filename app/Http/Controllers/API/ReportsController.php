<?php

namespace App\Http\Controllers\API;

use App\Http\Exports\UserPointsReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends InitController
{
    private $current;
    public function __construct()
    {
        parent::__construct();
        $this->current = Auth::guard('admin_api')->user();
        $this->middleware(['auth:admin_api','active_admin']);
    }

    public function UserPoints(Request $request){
        return Excel::download(new UserPointsReport($request), time().'-user_points_report.xlsx');
        //return response()->download(route('reports.user.points'));
    }
}
