<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Exports\UserPointsReport;
use Maatwebsite\Excel\Facades\Excel;

class PublicController extends InitController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function merchantTypes(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->getAll('Type',['type' => 'merchant']);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    
    public function categories(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->getAll('Category');
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function cities(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->getAll('Country', ['parent_id' => 1]);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function areas(Request $request, $id)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->getAll('Area', ['city_id' => $id]);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function merchants(Request $request)
    {
        $message = 'done.';
        $code = 200;
        $conditions = $whereIn = [];
        try {
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            if($request->filled('category_id')) {
                $conditions['category_id'] = $request->category_id;
            }
            if($request->filled('sponsor_id')) {
                $whereIn['key'] = 'id';
                $whereIn['values'] = $this->serviceObj->getAll('MerchantSponsor',['sponsor_id' => $request->sponsor_id])->pluck('merchant_id')->toArray();
            }
            $result = $this->serviceObj->getAll('Merchant',$conditions,['category','type','sponsors','branches'],25, $whereIn);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function branches(Request $request, $id)
    {
        $message = 'done.';
        $code = 200;
        $conditions = ['merchant_id' => $id,'status' => 1];
        try {
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            if($request->filled('area_id')) {
                $conditions['area_id'] = $request->area_id;
            }
            if($request->filled('city_id')) {
                $conditions['city_id'] = $request->city_id;
            }
            if($request->filled('merchant_id')) {
                $conditions['merchant_id'] = $request->merchant_id;
            }

            $merchant = $this->serviceObj->find('Merchant',['id' => $id,'status' => 1]);
            if(!$merchant) {
                throw new \Exception('Merchant not existed.', 404);
            }
            $result = $this->serviceObj->getAll('Branch',$conditions,['city', 'area'],25);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function merchant(Request $request, $id)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->find('Merchant',['id' => $id,'status' => 1],['category','type','sponsors','branches']);
            if(!$result) {
                throw new \Exception('Merchant not existed.', 404);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function branch(Request $request, $id)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->find('Branch',['id' => $id,'status' => 1],['merchant','city', 'area']);
            if(!$result) {
                throw new \Exception('Branch not existed.', 404);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function sponsors(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->getAll('Sponsor',['status' => 1, ['deleted_at','=',null]]);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function dataSearch(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result['sponsors'] = $this->serviceObj->getAll('Sponsor',['status' => 1]);
            $result['merchants'] = $this->serviceObj->find('Merchant',['status' => 1]);
            $result['categories'] = $this->serviceObj->getAll('Category');
            $result['cities'] = $this->serviceObj->getAll('Country', ['parent_id' => 1]);
            $result['vouchers'] = $this->serviceObj->getAll('Voucher',['status' => 1]);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function vouchers(Request $request)
    {
        $message = 'done.';
        $code = 200;
        $conditions = ['status' => 1];
        try {
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            if($request->filled('merchant_id')) {
                $conditions['merchant_id'] = $request->merchant_id;
            }
            if($request->filled('category_id')) {
                $conditions['category_id'] = $request->category_id;
            }
            if($request->filled('expired') && $request->expired) {
                $conditions[] = ['end_at', '<', \Carbon\Carbon::today()];
            } elseif ($request->filled('expired') && !$request->expired) {
                $conditions[] = ['end_at', '>=', \Carbon\Carbon::today()];
            }
            $result = $this->serviceObj->getAll('Voucher',$conditions,['branches', 'prices','merchant','category'],25);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }



    # ============= Reports
    public function UserPoints(Request $request){
        return Excel::download(new UserPointsReport($request), time().'-user_points_report.xlsx');
    }
}
