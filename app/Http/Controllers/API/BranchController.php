<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserVoucherRequest;

class BranchController extends InitController
{
    private $current;
    public function __construct()
    {
        parent::__construct();
        $this->current = Auth::guard('branch_api')->user();

        $this->middleware(['auth:branch_api','active_branch'])->except(['login','activate','forgetPassword','resetPassword']);
    }


    public function vouchers(Request $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $vouchers = $this->current->vouchers()->whereDate('end_at','>=',Carbon::today());
            if($request->expired)
                $vouchers = $this->current->vouchers()->whereDate('end_at','<',Carbon::today());

            $voucherIDs = $vouchers->get()->pluck('id')->toArray();
            $vouchers = $vouchers->get();

            $conditions = [];
            if($request->filled('redeemed'))
                $conditions['redeemed'] = 1;
            if($request->filled('user_id'))
                $conditions['user_id'] = (int)$request->user_id;
            $userVouchers = $this->serviceObj->getAll(
                'UserVoucher', $conditions, [], false,
                ['key' => 'voucher_id','values' => $voucherIDs]);

            $userVouchers->map(function($userVoucher) {
                $userVoucher->voucher_title_ar = $userVoucher->voucher->title_ar;
                $userVoucher->voucher_title_en = $userVoucher->voucher->title_en;
                $userVoucher->makeHidden(['voucher']);
            });

            if($request->filled('redeemed') || $request->filled('user_id'))
                $result = $userVouchers;
            else
                $result = $vouchers;
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function redeemVoucher(UserVoucherRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->only(['user_id','voucher_id']);
            $voucher = $this->serviceObj->find('Voucher', ['id' => $request->voucher_id]);
            $userVoucher = $this->serviceObj->find('UserVoucher', $data);

            $myVouchersIds = $this->current->vouchers->pluck('id')->toArray();
            if(!in_array($request->voucher_id, $myVouchersIds)) {
                throw new \Exception('Voucher not existed', 404);
            }
            if(!$userVoucher || $voucher->end_at < Carbon::today() || !$voucher->status) {
                throw new \Exception('Voucher isn\'t available', 400);
            }
            if($userVoucher->redeemed) {
                throw new \Exception('Voucher used before.');
            }
            if($voucher->pin_code != $request->pin_code) {
                throw new \Exception('Wrong PIN Code.');
            }
            $result = $this->serviceObj->update('UserVoucher', $data, ['redeemed' => 1]);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function me(Request $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $result = $this->current;
            $result['vouchers'] = $this->current->vouchers;
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    public function login(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $credentials = $request->only(['email','password']);
            $data = $this->serviceObj->find('Branch', ['email' => $request->email]);

            if(!$data['access_token'] = Auth::guard('branch_api')->attempt($credentials))
                throw new \Exception("Error Processing Request", 400);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $data = [];
        }
        return jsonResponse($code, $message, $data);
    }
}
