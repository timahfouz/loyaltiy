<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\AreaRequest;
use App\Http\Requests\BranchRequest;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CityRequest;
use App\Http\Requests\EventRequest;
use App\Http\Requests\MerchantRequest;
use App\Http\Requests\UserEventRequest;
use App\Http\Requests\UserVoucherRequest;
use App\Http\Requests\VoucherRequest;
use App\Http\Requests\UserPointsRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use PHPUnit\Util\Exception;

class AdminController extends InitController
{
    private $current;
    public function __construct()
    {
        parent::__construct();
        $this->current = Auth::guard('admin_api')->user();

        $this->middleware(['auth:admin_api','active_admin'])->except(['login','activate','forgetPassword','resetPassword']);
    }

    # ============ GET METHODS ================
    public function admins(Request $request)
    {
        $message = 'done.';
        $code = 200;
        $conditions = [];
        try {
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            if($request->filled('type_id')) {
                $conditions['type_id'] = $request->type_id;
            }
            $result = $this->serviceObj->getAll('Admin',$conditions,[],25);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function adminTypes(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $result = $this->serviceObj->getAll('Type',['type' => 'admin']);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function users(Request $request)
    {
        $message = 'done.';
        $code = 200;
        $conditions = ['status' => 1];
        try {
            if($this->current->type_name != 'super admin') {
                throw new \Exception('only super admin can view users list.',400);
            }

            if($request->filled('id')) {
                $conditions['id'] = $request->id;
            }
            if($request->filled('phone')) {
                $conditions[] = ['phone','LIKE','%'.$request->phone.'%'];
            }
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            if($request->filled('sponsor_id')) {
                $conditions['sponsor_id'] = $request->sponsor_id;
            }
            $result = $this->serviceObj->getAll('User',$conditions,['sponsor','transactions'],25);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function userInfo(Request $request, $id)
    {
        $message = 'done.';
        $code = 200;

        try {
            if($this->current->type_name != 'super admin') {
                throw new \Exception('only super admin can view user details.',400);
            }
            $result = $this->serviceObj->find('User',[],['sponsor','transactions']);
            if(!$result) {
                throw new \Exception('User not existed!', 404);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function events(Request $request)
    {
        $message = 'done.';
        $code = 200;
        $conditions = ['status' => 1];
        try {
            if($request->filled('status')) {
                $conditions['status'] = $request->status;
            }
            $result = $this->serviceObj->getAll('Event',$conditions,[],25);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    # ============ POST METHODS ================
    public function storeAdmin(AdminRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token','image', 'password_confirmation']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users');
            }
            $type = $this->serviceObj->find('Type', ['id' => $request->type_id]);
            if($this->current->type_name != 'super admin' && $type->name == 'super admin') {
                throw new \Exception('only super admin can create super admin.',400);
            }

            $email_token = time().Str::random(16);
            $data['email_token'] = $email_token;
            $result = $this->serviceObj->create('Admin', $data);
            if($result) {
                $from = env('MAIL_USERNAME');
                $data = [];
                $data['username'] = $request->name;
                $data['email'] = $request->email;
                $data['password'] = $request->password;
                $data['content'] = 'You\'ve been invited to be an admin in Zoboon System';
                $data['url'] = env('APP_URL');
                $data['reset_password_url'] = route('admin.activate',['email_token' => $email_token]);

                Mail::send('Admin.mails.template', $data, function ($mail) use($data, $from) {
                    $mail->from($from, 'Zoboon');
                    $mail->to($data['email'], 'Zoboon')->subject('Zoboon Invitiation');
                });
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage().' '.$e->getFile().' '.$e->getLine();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeMerchant(MerchantRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token','logo', 'password_confirmation','sponsors']);
            if($request->hasFile('logo')) {
                $data['logo'] = uploadFile($request->logo, 'images/merchants');
            }
            $result = $this->serviceObj->create('Merchant', $data);
            if($result) {
                if($request->filled('sponsors')) {
                    $sponsors = [];
                    foreach($request->sponsors as $sponsorID) {
                        $item['merchant_id'] = $result->id;
                        $item['sponsor_id'] = $sponsorID;
                        $sponsors[] = $item;
                    }
                    $this->serviceObj->bulkInsert('MerchantSponsor', $sponsors);
                }
            }

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeBranch(BranchRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token','image', 'password_confirmation',]);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/merchants');
            }
            $result = $this->serviceObj->create('Branch', $data);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeCategory(CategoryRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/categories');
            }
            $result = $this->serviceObj->create('Category', $data);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeCity(CityRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/flags');
            }
            $result = $this->serviceObj->create('Country', $data);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeArea(AreaRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token']);

            $result = $this->serviceObj->create('Area', $data);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeEvent(EventRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['name','description']);
            $data['ar'] = [
                'name' => $request->name['ar'],
                'description' => $request->description['ar'],
            ];
            $data['en'] = [
                'name' => $request->name['en'],
                'description' => $request->description['en'],
            ];
            $result = $this->serviceObj->create('Event', $data);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function storeVoucher(VoucherRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            $data = $request->except(['_token','image','branches','title','description','prices']);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/merchants');
            }
            $data['ar'] = [
                'title' => $request->title['ar'],
                'description' => $request->description['ar'],
            ];
            $data['en'] = [
                'title' => $request->title['en'],
                'description' => $request->description['en'],
            ];
            $result = $this->serviceObj->create('Voucher', $data);
            if($result) {
                $branches = $prices = [];
                foreach($request->branches as $branchID) {
                    $item = [];
                    $item['voucher_id'] = $result->id;
                    $item['branch_id'] = $branchID;
                    $branches[] = $item;
                }
                foreach($request->prices as $price) {
                    $item = [];
                    $item['voucher_id'] = $result->id;
                    $item['price'] = $price;
                    $prices[] = $item;
                }
                $this->serviceObj->bulkInsert('VoucherPrice', $prices);
                $this->serviceObj->bulkInsert('VoucherBranch', $branches);

                $result->prices;
                $result->branches;
            }

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function addPoints(UserPointsRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            if($this->current->type_name != 'super admin') {
                throw new \Exception('Only super admin can modify Users points.',400);
            }
            $data = $request->only(['user_id','points','description']);
            $user = $this->serviceObj->find('User', ['id' => $request->user_id]);
            if($request->points < 0 && $user->points < abs($request->points)) {
                throw new \Exception('User doesn\'t have this value of points');
            }
            $result = $this->serviceObj->create('UserPoint', $data);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function assignEvent(UserEventRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            if($this->current->type_name != 'super admin') {
                throw new \Exception('Only super admin can assign Users to Events.',400);
            }
            $data = $request->only(['user_id','event_id']);
            $event = $this->serviceObj->find('Event', ['id' => $request->event_id]);
            if(!$event->status) {
                throw new \Exception('Event isn\'t available');
            }
            $result = $this->serviceObj->create('UserEvent', $data);
            if($result) {
                $this->serviceObj->create('UserPoint', [
                    'user_id' => $request->user_id,
                    'points' => $event->points,
                    'description' => 'Points added from event: '.$event->translateOrNew('en')->name
                ]);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function assignVoucher(UserVoucherRequest $request)
    {
        $message = 'done.';
        $code = 201;
        try {
            if($this->current->type_name != 'super admin') {
                throw new \Exception('Only super admin can assign Users to Vouchers.',400);
            }
            $data = $request->only(['user_id','voucher_id']);
            $voucher = $this->serviceObj->find('Voucher', ['id' => $request->voucher_id]);
            if($voucher->end_at < Carbon::today() || !$voucher->status) {
                throw new \Exception('Voucher isn\'t available');
            }
            $result = $this->serviceObj->create('UserVoucher', $data);
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }

    # ============ PUT METHODS ================
    public function updateAdmin(AdminRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Admin', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Admin not found!', 404);
            }
            $data = $request->except(['image','_method','password_confirmation',]);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/users', true, $item->image);
            }
            if($request->filled('password')) {
                $data['password'] = bcrypt($request->password);
            }
            $type = $this->serviceObj->find('Type', ['id' => $request->type_id]);
            if($this->current->type_name != 'super admin' && $type->name == 'super admin') {
                throw new \Exception('only super admin can update super admin.',400);
            }
            if($this->current->id == $id && $request->status == 0) {
                throw new \Exception('You can\'t activate yourself',400);
            }
            $result = $this->serviceObj->update('Admin', ['id' => $id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            } else {
                if(!$item->status) {
                    $item->branches()->update(['status' => 0]);
                }
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateMerchant(MerchantRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $merchant = $this->serviceObj->find('Merchant', ['id' => $id]);
            if(!$merchant) {
                throw new \Exception('Merchant not found!', 404);
            }
            $data = $request->except(['image','_method','password_confirmation','sponsors']);
            if($request->hasFile('logo')) {
                $data['logo'] = uploadFile($request->logo, 'images/merchants', true, $merchant->logo);
            }
            if($request->filled('password')) {
                $data['password'] = bcrypt($request->password);
            }
            $result = $this->serviceObj->update('Merchant', ['id' => $id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            } else {
                if(!$merchant->status) {
                    $merchant->branches()->update(['status' => 0]);
                }
                if($request->filled('sponsors')) {
                    $this->serviceObj->destroy('MerchantSponsor', ['merchant_id' => $id]);
                    $sponsors = [];
                    foreach($request->sponsors as $sponsorID) {
                        $item['merchant_id'] = $result->id;
                        $item['sponsor_id'] = $sponsorID;
                        $sponsors[] = $item;
                    }
                    $this->serviceObj->bulkInsert('MerchantSponsor', $sponsors);
                }
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateBranch(BranchRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $merchant = $this->serviceObj->find('Branch', ['id' => $id]);
            if(!$merchant) {
                throw new \Exception('Branch not found!', 404);
            }
            $data = $request->except(['image','_method','password_confirmation',]);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->image, 'images/merchants', true, $merchant->image);
            }
            if($request->filled('password')) {
                $data['password'] = bcrypt($request->password);
            }
            $result = $this->serviceObj->update('Branch', ['id' => $id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateCategory(CategoryRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Category', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Category not found!', 404);
            }
            $data = $request->except(['image','_method',]);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->file('image'), 'images/categories', true, $item->image);
            }
            $result = $this->serviceObj->update('Category', ['id' => $id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateCity(CityRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Country', ['id' => $id, ['parent_id','!=',null]]);
            if(!$item) {
                throw new \Exception('City not found!', 404);
            }
            $data = $request->except(['image','_method',]);
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->file('image'), 'images/flags', true, $item->image);
            }
            $result = $this->serviceObj->update('Country', ['id' => $id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateArea(AreaRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Area', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Area not found!', 404);
            }
            $data = $request->except(['_method',]);

            $result = $this->serviceObj->update('Area', ['id' => $id], $data);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateEvent(EventRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Event', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Event not found!', 404);
            }
            $data = $request->except(['_method','name','description']);

            $result = $this->serviceObj->update('Event', ['id' => $id], $data);

            $result->translate('ar')->name = $request->name['ar'] ?? $item->translateOrNew('ar')->name;
            $result->translate('en')->name = $request->name['en'] ?? $item->translateOrNew('en')->name;
            $result->translate('ar')->description = $request->description['ar'] ?? $item->translateOrNew('ar')->description;
            $result->translate('en')->description = $request->description['en'] ?? $item->translateOrNew('ar')->description;
            $result->save();

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function updateVoucher(VoucherRequest $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Voucher', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Voucher not found!', 404);
            }
            $data = $request->except(['_method','image','branches','title','description','prices']);

            $result = $this->serviceObj->update('Voucher', ['id' => $id], $data);

            $result->translate('ar')->title = $request->title['ar'] ?? $item->translateOrNew('ar')->title;
            $result->translate('en')->title = $request->title['en'] ?? $item->translateOrNew('en')->title;
            $result->translate('ar')->description = $request->description['ar'] ?? $item->translateOrNew('ar')->description;
            $result->translate('en')->description = $request->description['en'] ?? $item->translateOrNew('en')->description;
            $result->save();

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            } else {
                $branches = $prices = [];
                if($request->filled('branches')) {
                    $result->voucherBranches()->delete();
                    foreach($request->branches as $branchID) {
                        $itemData = [];
                        $itemData['voucher_id'] = $result->id;
                        $itemData['branch_id'] = $branchID;
                        $branches[] = $itemData;
                    }
                    $this->serviceObj->bulkInsert('VoucherBranch', $branches);
                }
                if($request->filled('prices')) {
                    $result->prices()->delete();
                    foreach($request->prices as $price) {
                        $itemData = [];
                        $itemData['voucher_id'] = $result->id;
                        $itemData['price'] = $price;
                        $prices[] = $itemData;
                    }
                    $this->serviceObj->bulkInsert('VoucherPrice', $prices);
                }
                $result->prices;
                $result->branches;
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }


    # ============ Delete METHODS ================
    public function deleteCategory(Request $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Category', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Category not found!', 404);
            }
            $result = $this->serviceObj->destroy('Category', ['id' => $id]);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, []);
    }
    public function deleteCity(Request $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Country', ['id' => $id, ['parent_id','!=',null]]);
            if(!$item) {
                throw new \Exception('City not found!', 404);
            }
            $result = $this->serviceObj->destroy('Country', ['id' => $id]);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, []);
    }
    public function deleteArea(Request $request, $id)
    {
        $code = 201;
        $message = 'done.';
        try {
            $item = $this->serviceObj->find('Area', ['id' => $id]);
            if(!$item) {
                throw new \Exception('Area not found!', 404);
            }
            $result = $this->serviceObj->destroy('Area', ['id' => $id]);

            if(!$result) {
                throw new \Exception('something went wrong!', 400);
            }
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message, []);
    }


    public function activate(Request $request, $email_token)
    {
        $message = 'done.';
        $code = 200;
        try {
            $request->validate([
                'email' => 'required|exists:admins,email',
                'password' => 'nullable|min:6|confirmed'
            ]);
            $data = ['email_token' => null, 'status' => 1];
            if($request->filled('password'))
                $data['password'] = bcrypt($request->password);

            $result = $this->serviceObj->find('Admin', ['email' => $request->email, 'email_token' => $email_token]);

            if(!$result)
                throw new \Exception("not existed", 404);

            $this->serviceObj->update('Admin',['email' => $request->email, 'email_token' => $email_token],$data);
            $result = $this->serviceObj->find('Admin', ['email' => $request->email]);
            $result['access_token'] = Auth::guard('admin_api')->login($result);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function forgetPassword(Request $request)
    {
        $message = 'check your email.';
        $code = 200;
        try {
            $request->validate([
                'email' => 'required|exists:admins,email',
            ]);
            $admin = $this->serviceObj->find('Admin', ['email' => $request->email]);
            if(!$admin)
                throw new \Exception("not existed", 404);

            $email_token = time().Str::random(16);
            $data['email_token'] = $email_token;
            $this->serviceObj->update('Admin',['email' => $request->email],$data);

            $from = env('MAIL_USERNAME');
            $data = [];
            $data['username'] = $admin->name;
            $data['email'] = $admin->email;

            $data['content'] = 'Click the button below to change your password';
            $data['url'] = env('APP_URL');
            $data['reset_password_url'] = route('admin.reset.password',['email_token' => $email_token]);

            Mail::send('Admin.mails.template', $data, function ($mail) use($data, $from) {
                $mail->from($from, 'Zoboon');
                $mail->to($data['email'], 'Zoboon')->subject('Reset Password');
            });
        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
        }
        return jsonResponse($code, $message);
    }
    public function resetPassword(Request $request, $email_token)
    {
        $message = 'done.';
        $code = 200;
        try {
            $request->validate([
                'email' => 'required|exists:admins,email',
                'password' => 'required|min:6|confirmed'
            ]);
            $data = ['email_token' => null];
            $data['password'] = bcrypt($request->password);

            $result = $this->serviceObj->find('Admin', ['email' => $request->email, 'email_token' => $email_token]);
            if(!$result)
                throw new \Exception("not existed", 404);

            $this->serviceObj->update('Admin',['email' => $request->email, 'email_token' => $email_token],$data);
            $result = $this->serviceObj->find('Admin', ['email' => $request->email]);
            $result['access_token'] = Auth::guard('admin_api')->login($result);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $result = [];
        }
        return jsonResponse($code, $message, $result);
    }
    public function login(Request $request)
    {
        $message = 'done.';
        $code = 200;
        try {
            $credentials = $request->only(['email','password']);
            $data = $this->serviceObj->find('Admin', ['email' => $request->email]);

            if(!$data['access_token'] = Auth::guard('admin_api')->attempt($credentials))
                throw new \Exception("Error Processing Request", 1);

        } catch (\Exception $e) {
            $code = responseCode($e->getCode());
            $message = $e->getMessage();
            $data = [];
        }
        return jsonResponse($code, $message, $data);
    }
}
