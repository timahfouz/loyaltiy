<?php

use App\Http\Services\CommonService;
use Illuminate\Support\Str;


function jsonResponse($code = 200, $message = '', $data = []) {
    return response()->json([
        'code' => $code,
        'message' => $message,
        'data' => $data ?? [],
    ],$code);
}

function responseCode($code) {
    return in_array($code, [200,201,401,403,404,500]) ? $code : 400;
}


function uploadFile($file, $path, $edit = false, $oldFile = null) {
    $destination = env('SYSTEM_PATH')().'/'.$path;
    $oldDestination = env('SYSTEM_PATH')().'/'.$path.'/'.$oldFile;
    if($edit && is_file($oldDestination))
        unlink($oldDestination);
    $ext = $file->getClientOriginalExtension();
    $name = time().Str::random(5);
    $fileName = $name.'.'.$ext;
    $file->move($destination, $fileName);
    return $fileName;
}

function generateCode($count, $model = false, $numbersOnly = false) {
    $code = $numbersOnly ? rand(((int)$count - 1)*10, pow(10, (int)$count)-1) : strToUpper(Str::random(((int)$count)));
    if($model) {
        $serviceObj = new CommonService;
        $exist = $serviceObj->find($model,['code' => $code]);
        if($exist)
            generateCode();
    }
    return $code;
}



/*function uploadFile($file, $destination, $edit = false, $oldPath = null) {
    $ext  = $file->getClientOriginalExtension();
    $path = env('SYSTEM_PATH')($destination);

    $name = time().str_random(12).'.'.$ext;
    $file->move($path, $name);
    if($edit) {
        $oldfile = env('SYSTEM_PATH')($destination).'/'.$oldPath;
        if(is_file($oldfile))
            unlink($oldfile);
    }
    return $name;
}*/