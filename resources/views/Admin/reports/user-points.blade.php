<table>
    <thead>
    <tr>
        <th>User ID</th>
        <th>Network</th>
        <th>Available Points</th>
        <th>Total points over the time</th>
        <th>Spent points over the time</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->network }}</td>
            <td>{{ $item->available_points }}</td>
            <td>{{ $item->total_points_over_the_time }}</td>
            <td>{{ $item->spent_points_over_the_time }}</td>
        </tr>
    @endforeach
    </tbody>
</table>