<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('merchant_id')->unsigned();
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->string('image');
            #$table->string('title');
            #$table->text('description');
            $table->double('cash_cost')->nullable();
            $table->integer('pin_code',4)->unique();
            #$table->bigInteger('points');
            $table->boolean('status')->default(1);
            $table->timestamp('start_at')->default(\Carbon\Carbon::now());
            $table->timestamp('end_at');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('merchant_id')->references('id')->on('merchants')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
