<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type_id')->unsigned()->nullable();
            $table->bigInteger('category_id')->unsigned()->nullable();
            $table->string('name_ar')->unique();
            $table->string('name_en')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('username')->unique();
            $table->string('logo')->nullable();
            $table->bigInteger('balance')->default(0);
            $table->string('address')->nullable();
            $table->bigInteger('max_receipt_value')->default(1000000000);
            $table->text('description')->nullable();
            $table->text('terms')->nullable();
            $table->boolean('enable_points')->default(1);
            $table->boolean('enable_vouchers')->default(1);
            $table->boolean('status')->default(1);
            $table->boolean('blocked')->default(0);
            $table->string('password');
            $table->string('firebase')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('type_id')->references('id')->on('types')->onUpdate('cascade')->onDelete('set null');
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
